<?php
if (!defined('xDEC')) exit;
class Cookie
{
    public function setCookie($key, $value, $time = 604800, $path = '/')
    {
        return setcookie($key, $value, time() + $time, $path);
    }

    public function setSecureCookie($key, $value, $time = 604800, $path = '/')
    {
        return setcookie($key, saltSHA1SecretCreate($value), time() + $time, $path);
    }

    public function removeCookie($key)
    {
        return setcookie($key, null, time() - 604800, null, null, true);
    }

    public function getCookie($key)
    {
        if (isset($_COOKIE[$key])) {
            return $_COOKIE[$key];
        }
        return null;
    }

    public function getSecureCookie($key)
    {
        if (isset($_COOKIE[$key])) {
            $val = saltSHA1SecretGetValue($_COOKIE[$key]);
            if ($val == null) $this->removeCookie($key);
            return $val;
        }
        return null;
    }
}