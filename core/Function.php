<?php
if (!defined('xDEC')) exit;
/**
 * @param $key
 * @return mixed
 */
function get($key)
{
    return Registry::get($key);
}

function _get(&$haystack, $needle)
{
    if (isset($haystack[$needle])) return $haystack[$needle];
    return null;
}

/**
 * @param $key
 * @param $value
 * @return int
 */
function set($key, $value)
{
    return Registry::set($key, $value);
}

/**
 * @param $key
 */
function remove($key)
{
    Registry::remove($key);
}

/**
 * @return bool
 */
function is_ssl()
{
    if (isset($_SERVER['HTTPS'])) {
        if ('on' == strtolower($_SERVER['HTTPS']))
            return true;
        if ('1' == $_SERVER['HTTPS'])
            return true;
    } elseif (isset($_SERVER['SERVER_PORT']) && ('443' == $_SERVER['SERVER_PORT'])) {
        return true;
    }
    return false;
}

/**
 * @param $errno
 * @param $errstr
 * @param $errfile
 * @param $errline
 */
function error_handler($errno, $errstr, $errfile, $errline, $econtext)
{
    switch ($errno) {
        case E_CORE_ERROR:
            $logger = get('Logger');
            if ($logger instanceof Logger) $logger->error(
                print_r(array(
                    "ERROR NUMBER: " => $errno,
                    "ERROR STRING: " => $errstr,
                    "ERROR FILE: " => $errfile,
                    "ERROR LINE: " => $errline,
                    "ERROR DETAILS: " => $econtext
                ), true)
            );
            exit;
            break;
        default:
            $logger = get('Logger');
            if ($logger instanceof Logger) $logger->error(
                print_r(array(
                    "ERROR TYPE:" => "UNKNOWN",
                    "ERROR NUMBER: " => $errno,
                    "ERROR STRING: " => $errstr,
                    "ERROR FILE: " => $errfile,
                    "ERROR LINE: " => $errline,
                    "ERROR DETAILS: " => $econtext
                ), true)
            );
            break;
    }

}

/**
 * @param $directory
 * @return int
 */
function directory_size($directory)
{
    $directorySize = 0;
    if ($dh = @opendir($directory)) {
        while (($filename = readdir($dh))) {
            if ($filename != "." && $filename != "..") {
                if (is_file($directory . "/" . $filename))
                    $directorySize += filesize($directory . "/" . $filename);
                if (is_dir($directory . "/" . $filename))
                    $directorySize += directory_size($directory . "/" . $filename);
            }
        }
    }
    @closedir($dh);
    return $directorySize;
}

function quot($str, $enc = '`')
{
    return $enc . $str . $enc;
}

function arrayToString($array, $glue, $hack = null)
{
    if (!is_array($array)) {
        return $array;
    }
    if ($hack) {
        return $hack . implode($glue, $array) . $glue;
    }
    return implode($glue, $array);
}

function random($count)
{
    return substr(sha1(time()), 0, $count);
}

function saltSHA1Create($string, $random = null)
{
    if (null == $random) $random = random(8);
    return $string . '|' . sha1($string . $random) . ':' . $random;
}

function saltSHA1SecretCreate($string, $random = null)
{
    if (null == $random) $random = random(8);
    return $string . '|' . sha1(SECRET_KEY . $string . $random) . ':' . $random;
}

function saltSHA1Check($string)
{
    $hash = explode('|', $string);
    if (count($hash) >= 2) {
        $key = $hash[count($hash) - 1];
        $rand = explode(':', $hash[1]);
        if (count($rand) == 2)
            return $string == saltSHA1Create($key, $rand[1]);
    }
    return false;
}

function saltSHA1SecretCheck($string)
{
    $hash = explode('|', $string);
    if (count($hash) >= 2) {
        $key = $hash[count($hash) - 1];
        $rand = explode(':', $hash[1]);
        if (count($rand) == 2)
            return $string == saltSHA1Create(SECRET_KEY . $key, $rand[1]);
    }
    return false;
}

function saltSHA1GetValue($string)
{
    $hash = explode('|', $string);
    if (count($hash) >= 2) {
        $key = $hash[count($hash) - 1];
        return substr($string, 0, strlen($string) - strlen($key) - 1);
    }
    return null;
}

function saltSHA1SecretGetValue($string)
{
    return saltSHA1GetValue($string);
}

function passwordHash($string, $random = null)
{
    $p = explode('|', saltSHA1SecretCreate($string, $random));
    return $p[count($p) - 1];
}

function passwordCheck($hash, $pass)
{
    $random = explode(':', $hash);
    return $hash == passwordHash($pass, $random[1]);
}

function staticUrl($chunk)
{
    return URL_STATIC . $chunk;
}

function push(&$array, $key, $val)
{
    if (array_key_exists($key, $array)) {
        $array[$key] .= ' ' . $val;
    } else $array[$key] = $val;
}

function _push(&$array, $key, $val)
{
    if (array_key_exists($key, $array)) {
        if (is_array($array[$key])) array_push($array[$key], $val); else $array[$key] = array($array[$key], $val);
    } else $array[$key] = $val;
}

function encrypt($plainText)
{
    return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(SECRET_KEY), $plainText, MCRYPT_MODE_CBC, md5(md5(SECRET_KEY))));
}

function decrypt($cipherText)
{
    return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(SECRET_KEY), base64_decode($cipherText), MCRYPT_MODE_CBC, md5(md5(SECRET_KEY))), "\0");
}

function toString($array, $tab = '')
{
    $str = '';
    foreach ($array as $k => $val) {
        $str .= $k . ": " . (is_array($val) ? toString($val, $tab . '    ') : $val) . PHP_EOL;
    }
    return $str;
}