<?php
if (!defined('xDEC')) exit;
/**
 * Class Database
 * @package xDec
 * @description MySQL database handler
 */
class Database
{
    /**
     * @var bool
     */
    private $lock = true;
    private $success = true;

    private $timer;

    /**
     * @var
     */
    /**
     * @var
     */
    private $_connection, $_result;

    /**
     * @var
     */
    private $insert_id;

    /**
     * @return mixed
     */
    public function insertId()
    {
        return $this->insert_id;
    }

    /**
     *
     */
    public function __construct()
    {
        if (!(defined('DB_HOST') && defined('DB_USER') && defined('DB_PASS') && defined('DB_NAME')))
            trigger_error("Check `config.ini.php` for database connection definitions", E_USER_ERROR);
    }

    /**
     *
     */
    function __destruct()
    {
        $this->close();
    }

    /**
     * @param $table
     * @param $fields
     * @param $condition
     * @param $args
     */
    public function select($table, $fields, $condition, $args)
    {
        if ($this->lock && null == $this->_connection)
            $this->connect();
        if ($this->_connection instanceof PDO) {
            if (is_array($table)) {
                $table = '`' . implode('`, `', $table) . '`';
            } else $table = ($table);
            if (is_array($fields)) {
                $fields = '`' . implode('`, `', $fields) . '`';
            } else $fields = ($fields);
            try {
                $this->timer = microtime();
                $this->_result = $statement = $this->_connection->prepare("SELECT {$fields} FROM {$table} {$condition}");
                $this->success = $statement->execute($args);
                if (LOG_DB_QUERY) get('Logger')->sql(
                    "TIME-TAKEN: " . (microtime() - $this->timer) . PHP_EOL .
                    "QUERY-STRING: " . $statement->queryString . PHP_EOL .
                    "DATA: " . print_r(array_values($args), true) . PHP_EOL .
                    "RESULTS: " . $statement->rowCount()
                );
            } catch (PDOException $e) {
                get('Logger')->log(print_r($e, true));
                $this->_result = null;
            }
        } else {

        }
    }

    /**
     * @param $table
     * @param $data
     * @throws InvalidArgumentException
     */
    public function insert($table, $data)
    {
        if ($this->lock && null == $this->_connection)
            $this->connect();
        if ($this->_connection instanceof PDO) {
            if (!is_array($data)) throw new InvalidArgumentException('Invalid argument passed to insert: expected - string, array');
            $cols = '`' . implode('`, `', array_keys($data)) . '`';
            $values = implode(', ', array_fill(0, count($data), '?'));
            try {
                $this->timer = microtime();
                $this->_result = $statement = $this->_connection->prepare("INSERT INTO {$table}( {$cols} ) VALUES({$values})");
                $this->success = $statement->execute(array_values($data));
                $this->insert_id = $this->_connection->lastInsertId();
                if (LOG_DB_QUERY) get('Logger')->sql(
                    "TIME-TAKEN: " . (microtime() - $this->timer) . PHP_EOL .
                    "QUERY-STRING: " . $statement->queryString . PHP_EOL .
                    "DATA: " . print_r(array_values($data), true) . PHP_EOL .
                    "RESULTS: " .  $statement->rowCount()
                );
            } catch (PDOException $e) {
                get('Logger')->log(print_r($e, true));
                $this->_result = null;
            }
        } else {

        }
    }

    public function error()
    {
        return !$this->success;
    }

    public function errorCode()
    {
        if ($this->_result instanceof PDOStatement) {
            return $this->_result->errorCode();
        }
        return '';
    }

    public function errorInfo()
    {
        if ($this->_result instanceof PDOStatement) {
            return $this->_result->errorInfo();
        }
        return array();
    }

    /**
     * @param $table
     * @param $change
     * @param $condition
     * @param $args
     * @throws InvalidArgumentException
     */
    public function update($table, $change, $condition, $args)
    {
        if ($this->lock && null == $this->_connection)
            $this->connect();
        if ($this->_connection instanceof PDO) {
            if (!is_array($change)) throw new InvalidArgumentException('Invalid argument passed to insert: expected - string, array');

            try {
                $this->timer = microtime();
                $this->_result = $statement = $this->_connection->prepare("UPDATE {$table} SET {} {$condition}");
                $this->success = $statement->execute($args);
                if (LOG_DB_QUERY) get('Logger')->sql(
                    "TIME-TAKEN: " . (microtime() - $this->timer) . PHP_EOL .
                    "QUERY-STRING: " . $statement->queryString . PHP_EOL .
                    "DATA: " . print_r(array_values($args), true) . PHP_EOL .
                    "RESULTS: " . $statement->rowCount()
                );
            } catch (PDOException $e) {
                get('Logger')->log(print_r($e, true));
                $this->_result = null;
            }
        } else {

        }
    }

    /**
     * @param $table
     * @param $condition
     * @param $args
     */
    public function delete($table, $condition, $args)
    {
        if ($this->lock && null == $this->_connection)
            $this->connect();
        if ($this->_connection instanceof PDO) {
            try {
                $this->timer = microtime();
                $this->_result = $statement = $this->_connection->prepare("DELETE FROM {$table} {$condition}");
                $this->success = $statement->execute($args);
                if (LOG_DB_QUERY) get('Logger')->sql(
                    "TIME-TAKEN: " . (microtime() - $this->timer) . PHP_EOL .
                    "QUERY-STRING: " . $statement->queryString . PHP_EOL .
                    "DATA: " . print_r(array_values($args), true) . PHP_EOL .
                    "RESULTS: " . $statement->rowCount()
                );
            } catch (PDOException $e) {
                get('Logger')->log(print_r($e, true));
                $this->_result = null;;
            }
        } else {

        }
    }

    /**
     * @return mixed
     */
    public function result()
    {
        return $this->_result;
    }

    /**
     * @return mixed|null
     */
    public function row()
    {
        if ($this->_result instanceof PDOStatement)
            return $this->_result->fetch();
        return null;
    }

    /**
     * @return int
     */
    public function num_rows()
    {
        if ($this->_result instanceof PDOStatement)
            return $this->_result->rowCount();
        return -1;
    }

    /**
     * @param $str
     * @param $args
     */
    public function query($str, $args)
    {
        if ($this->lock && null == $this->_connection)
            $this->connect();
        if ($this->_connection instanceof PDO) {
            try {
                $this->_result = $statement = $this->_connection->prepare($str);
                $this->success = $statement->execute($args);
                if (LOG_DB_QUERY) get('Logger')->sql(
                    "TIME-TAKEN: " . (microtime() - $this->timer) . PHP_EOL .
                    "QUERY-STRING: " . $statement->queryString . PHP_EOL .
                    "DATA: " . print_r(array_values($args), true) . PHP_EOL .
                    "RESULTS: " . $statement->rowCount()
                );
            } catch (PDOException $e) {
                get('Logger')->log(print_r($e, true));
                $this->_result = null;
            }
        } else {

        }
    }

    /**
     * @throws BadFunctionCallException
     */
    private function connect()
    {
        if (defined('DB_ENABLED') && !DB_ENABLED) {
            throw new BadFunctionCallException('Database is disabled in config file');
        }
        try {
            $this->_connection = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS);
        } catch (PDOException $e) {
            $this->lock = false;
            get('Logger')->log(print_r($e, true));
        }
    }

    /**
     *
     */
    private function close()
    {
        if ($this->_connection instanceof PDO) $this->_connection = null;
    }

    /**
     *
     */
    private function __clone()
    {
    }
}

;

class DatabaseCache
{
}