<?php
if (!defined('xDEC')) exit;
abstract class Pages implements Page
{
function __head__($var)
{
    header('Content-Type: text/html; charset=utf-8');
    echo '<!DOCTYPE HTML><html lang="en" xmlns="http://www.w3.org/1999/html"><head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">';
    echo '<script>';
    @readfile(CONTENT.'js/jquery.js');
    echo '</script>';
    echo '<base href="http://' . DOMAIN_NAME . '/"/>';
}

function __title__($var)
{
    echo '<title>' . SITE_NAME . $var . '</title>';
}

function __body__($var)
{
    echo '</head><body>';
}

function end_body()
{
?><div id="alert"></div></body></html><?php
}

abstract function startOutput($var);
}