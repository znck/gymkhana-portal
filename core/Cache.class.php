<?php
if (!defined('xDEC')) exit;
class Cache
{
    function clean($f)
    {
        if (file_exists($f)) {
            get("Logger")->custom_log("cache.log",
                "CACHE  CLEARING" . PHP_EOL . "CACHE FILE: " . $f
            );
            unlink($f);
            unlink($f . ".zip");
            unlink($f . ".txt");
        }
    }

    function inCache()
    {
        $url = get(CACHE_FILE);
        return (get(CACHED) && file_exists(CACHE . md5($url) . ".dat"));
    }

    function get()
    {
        $url = get(CACHE_FILE);
        if ($this->inCache($url))
            return @file_get_contents(CACHE . md5($url) . ".dat");
        return '';
    }

    function _echo()
    {
        $url = get(CACHE_FILE);
        $f = CACHE . md5($url) . ".dat";
        $m = file_get_contents($f . ".txt");
        $meta = unserialize($m);
        if ($this->inCache($url)) {
            if ((isset($_SERVER['HTTP_USER_AGENT']) && strstr($_SERVER['HTTP_USER_AGENT'], 'W3C_Validator') !== false) || (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') === false)) {
                header("Content-length: " . filesize($f));
                @readfile($f);
            } else {
                header("Content-Encoding: gzip");
                header("Content-length: " . filesize($f . ".zip"));
                @readfile($f . ".zip");
            }
        }
        if ((date(DATE_RFC2822) < $meta['expire'])) {
            $this->clean($f);
        }
    }

    function startCaching()
    {
        if (false != ob_get_length())
            ob_clean();
        ob_start();
    }

    function stopCaching($d)
    {
        $url = get(CACHE_FILE);
        $f = CACHE . md5($url) . ".dat";
        $content = (ob_get_contents());
        ob_end_clean();
        $this->clean($f);
        file_put_contents($f, $content);
        $comp = gzencode($content);
        file_put_contents($f . ".zip", $comp);
        $a = array(
            'created' => date(DATE_RFC2822),
            'expire' => date(DATE_RFC2822, time() + 30 * 24 * 60 * 60),
            'url' => $d,
            'file' => $url
        );
        file_put_contents($f . ".txt", serialize($a));
        get("Logger")->custom_log("cache.log", "CACHING REQUEST: " . print_r($a, true));
        if ((isset($_SERVER['HTTP_USER_AGENT']) && strstr($_SERVER['HTTP_USER_AGENT'], 'W3C_Validator') !== false) || (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') === false)) {
            echo $content;
        } else {
            header("Content-Encoding: gzip");
            echo $comp;
        }
    }
}