<?php
if (!defined('xDEC')) exit;
/**
 *
 */
define('LOG_FILE', 'xDec.log');
/**
 *
 */
define('ERROR_LOG_FILE', 'xDecErrors.log');

define('SQL_LOG_FILE', 'sql.log');
/**
 * Class Logger
 */
class Logger
{
    private function backTraceText($arr, $class = array('Logger'))
    {
        $str = '';
        foreach ($arr as $a) {
            if ((isset($a['class']) && array_key_exists($a['class'], $class) )) {
                $str = "From line number #" . $a['line'] . " in " . $a['file'];
            } else break;
        }
        return $str;
    }

    private function backTraceTextFull($arr)
    {
        $str = '';
        foreach ($arr as $a) {
            $str .= "Line #" . $a['line'] . " in " . $a['file'] . PHP_EOL;
        }
        return $str;
    }

    /**
     * @param $file
     * @param $log
     */
    public function custom_log($file, $log)
    {
        $fp = @fopen(LOG . $file, 'a');
        @fwrite($fp, "TIMESTAMP: " . date(DATE_RFC2822) . PHP_EOL . $log . PHP_EOL .
            "Logged by: " . $this->backTraceText(debug_backtrace(2)) . SEP
        );
        @fclose($fp);
    }

    /**
     * @param $log
     */
    public function log($log)
    {
        $this->custom_log(LOG_FILE, $log);
    }

    /**
     * @param $log
     */
    public function error($log)
    {
        $fp = @fopen(LOG . ERROR_LOG_FILE, 'a');
        @fwrite($fp, "TIMESTAMP: " . date(DATE_RFC2822) . PHP_EOL . $log . PHP_EOL .
            "Logged by: " . $this->backTraceTextFull(debug_backtrace(2)) . SEP
        );
        @fclose($fp);
    }

    public function sql($log)
    {
        $fp = @fopen(LOG . SQL_LOG_FILE, 'a');
        @fwrite($fp, "TIMESTAMP: " . date(DATE_RFC2822) . PHP_EOL . $log . PHP_EOL .
            "Logged by: (sql) " . $this->backTraceText(debug_backtrace(2), array('Logger' => 1, 'Database' => 1)) . SEP
        );
        @fclose($fp);
    }
}