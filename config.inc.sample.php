<?php

define('DISABLED', false);
define('DISABLED_PAGE', '');

define('DOMAIN_NAME', '');

define('DB_HOST', '');
define('DB_USER', '');
define('DB_PASS', '');
define('DB_NAME', '');
define('DB_PORT', 3306);
define('DB_ENABLED', true);
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', 'utf8_unicode_ci');
define('LOG_DB_QUERY', true);

define('SECRET_KEY', '');

define('SITE_NAME', '');
define('SITE_META', '');

define('GMAIL_USER', '');
define('GMAIL_PASS', '');

define('BASE', dirname(__FILE__) . '/');