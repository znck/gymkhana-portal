<?php
/**
 * Developer: Rahul Kadyan
 * Date: 06/08/13
 * Time: 4:13 PM
 * Product: JetBrains PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

//For authentication and login form
class home extends Admin
{
    public function allowed($method, $user)
    {
        switch ($method) {
            case 'index':
            case 'login':
            case 'logout':
                return true;
        }
        return false;
    }

    function __head__($var)
    {
        parent::__head__($var);
        ?>
        <script src="<?php echo get('static_url'); ?>content/admin/jquery.js"></script>
        <script src="<?php echo get('static_url'); ?>content/admin/admin.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo get('static_url'); ?>content/admin/theme.css">
        <script>
            window.home_url = '<?php echo get('home_url'); ?>';
        </script>
    <?php
    }

    function __title__($var)
    {
        $title = '';
        switch ($var) {
            case 'index':
                $title .= 'Home';
                break;
            case 'login':
                $title .= 'Login';
                break;
        }
        return $title;
    }

    function index($var)
    {
        if (get('Auth')->logged()) {
            // TODO: think it
            $this->user();
            require_once(CONTENT . 'admin/index.php');
            list_my_files();
            list_my_requests();
            $this->create($var);
            $this->showDelete();
            ?>
            <div id="alert"></div>
            <?php
            $user = $this->getPermissions();
            if ($user['all']) {
                list_all_requests();
            }
        } else {
            require_once(CONTENT . 'admin/index.php');
            login_form();
        }
    }

    private function user()
    {
        get('Database')->select(
            Person::$name,
            '*',
            "WHERE ?=?",
            array(
                Person::$field_id,
                get('Auth')->logged_id()
            )
        );
        $user = get('Database')->row();
        ?>
        <div class="line">
            <div class="user">
                <div id="main-menu" class="menu">
                </div>
                <span onclick=""
                      style="cursor: pointer"><?php echo $user[Person::$field_name]; ?></span>
                | <a href="<?php echo get('home_url'); ?>home/logout">logout</a></div>
        </div>
        <div id="response"></div> <?php
    }

    function login($var)
    {
        if (isset($_POST['username']) && isset($_POST['password']))
            get('Auth')->login($_POST['username'], $_POST['password']);
        header('Location: ' . get('home_url'));
    }

    function logout($var)
    {
        get('Auth')->logout();
        header('Location: ' . get('home_url'));
    }

    function __meta__($var)
    {
        switch ($var) {
            case 'form':
            case 'login':
            case 'logout':
            case 'list_users':
            case 'reject':
            case 'approve':
                return 'independent';
            default:
                return null;
        }
    }

    function create($var)
    {
        require_once(CONTENT . 'admin/index.php');
        $user = $this->getPermissions();
        $this->create_user($var);

        if ($user['all'] || (array_key_exists('home.create.procedure', $user['perms']) && $user['perms']['home.create.procedure'])) {
            ?>
            <div id="create_proc" class="element">
                <h1>Create new approving procedure</h1>

                <div class="border">
                    <?php
                    create_procedure();
                    ?>
                    <div id="person_container">
                        <?php
                        list_all_users();
                        ?>
                    </div>
                </div>
            </div>

            <script>
                $(document).ready(
                    function () {
                        $('#main-menu').append('<span class="item" onclick="show(\'#create_proc\')">Create new approving procedure</span> ')
                    }
                )
            </script>
        <?php
        }

        $this->create_file($var);


    }

    function list_users($var)
    {
        $user = $this->getPermissions();
        if ($user['all'] || (array_key_exists('home.create.file', $user['perms']) && $user['perms']['home.create.file'])) {
            require_once(CONTENT . 'admin/index.php');
            list_all_users();
        }
    }

    function create_user($var)
    {
        $user = $this->getPermissions();
        if ($user['all'] || (array_key_exists('home.create.user', $user['perms']) && $user['perms']['home.create.user'])) {
            require_once(CONTENT . 'admin/index.php');
            create_user();
        }
    }

    function create_file($var)
    {
        $user = $this->getPermissions();
        if ($user['all'] || (array_key_exists('home.create.file', $user['perms']) && $user['perms']['home.create.file'])) {
            require_once(CONTENT . 'admin/index.php');
            create_file();
        }
    }

    function form($var)
    {
        switch ($var) {
            case 'user':
                // Checking if all required variables set...
                $user = $this->getPermissions();
                if ($user['all'] || (array_key_exists('home.create.user', $user['perms']) && $user['perms']['home.create.user'])) {

                    if (isset($_POST['username']) &&
                        isset($_POST['name']) &&
                        isset($_POST['password']) &&
                        isset($_POST['group']) &&
                        isset($_POST['post']) &&
                        preg_match('^[a-zA-Z0-9_-\.]{3,20}$', $_POST['username']) &&
                        preg_match('^.{3,20}$', $_POST['password']) &&
                        strlen($_POST['name']) > 2 &&
                        strlen($_POST['group']) > 2 &&
                        strlen($_POST['post']) > 2
                    ) {
                        $perm_str = array();
                        if (isset($_POST['home_*'])) $perm_str['home.*'] = true;
                        if (isset($_POST['home_create_*'])) $perm_str['home.create.*'] = true;
                        if (isset($_POST['home_create_file'])) $perm_str['home.create.file'] = true;
                        if (isset($_POST['home_create_user'])) $perm_str['home.create.user'] = true;
                        if (isset($_POST['home_create_procedure'])) $perm_str['home.create.procedure'] = true;
                        get('Database')->select(
                            Login::$name,
                            array(
                                Login::$field_user
                            ),
                            "WHERE ?='?'",
                            array(
                                Login::$field_user,
                                $_POST['username']
                            )
                        );
                        if (get('Database')->num_rows() > 0) {
                            echo 'User already exists.';
                        } else {
                            get('Database')->insert(
                                Login::$name,
                                array(
                                    Login::$field_user => $_POST['username'],
                                    Login::$field_pass => sha1($_POST['password'])
                                )
                            );
                            $id = get('Database')->insertId();
                            get('Database')->insert(
                                Person::$name,
                                array(
                                    Person::$field_name => $_POST['name'],
                                    Person::$field_post => $_POST['post'],
                                    Person::$field_power => json_encode($perm_str),
                                    Person::$field_id => $id,
                                    Person::$field_group => $_POST['group']
                                )
                            );
                            echo 'User added :)';
                        }
                    } else echo 'All fields are required. Try again!!!';
                } else echo 'You are not authorised for this action';
                break;
//            case 'delete':
//                $user = $this->getPermissions();
//                if ($user['del'] || (array_key_exists('home.delete.user', $user['perms']) && $user['perms']['home.delete.user'])) {
//                    if(isset($_POST['user_id']) && intval($_POST['user_id']) > 0){
//                        get('Database')->delete(
//                            Login::$name,
//                            "WHERE ?=?",
//                            array(
//                                Login::$field_id,
//                                $_POST['user_id']
//                            )
//                        );
//                    } else {
//                        echo 'Some error occurred';
//                    }
//                } else echo 'You are not authorised for this action';
//                break;
            case 'procedure':
                $user = $this->getPermissions();
                if ($user['all'] || (array_key_exists('home.create.procedure', $user['perms']) && $user['perms']['home.create.procedure'])) {
                    if (isset($_POST['title']) && isset($_POST['process']) && strlen($_POST['title']) > 3 && strlen($_POST['process']) > 1) {
                        get('Database')->insert(
                            '`' . Procedure::$name . '`',
                            array(
                                Procedure::$field_title => $_POST['title'],
                                Procedure::$field_process => $_POST['process']
                            )
                        );
                        echo 'Okay';
                    } else  echo 'some error occurred';
                } else echo 'You are not authorised for this action';
                break;
            case 'file':
                $user = $this->getPermissions();
                if ($user['all'] || (array_key_exists('home.create.file', $user['perms']) && $user['perms']['home.create.file'])) {
                    $err = false;
                    if (isset($_POST['procedure_id']) && isset($_POST['subject']) && isset($_POST['requirements']) && isset($_POST['details']) && isset($_POST['budget'])) {
                        if (strlen($_POST['subject']) < 5) {
                            $err = true;
                            echo '<b>Subject</b>: It should be a little more descriptive.<br>';
                        }
                        if (strlen($_POST['requirements']) < 5) {
                            $err = true;
                            echo '<b>Requirements</b>: A brief description of your requirements is helpful<br>';
                        }
                        if (strlen($_POST['details']) < 16) {
                            $err = true;
                            echo '<b>Details</b>: Make it a little more detailed.<br>';
                        }
                        if (strlen($_POST['budget']) < 1 || intval($_POST['budget']) < 0) {
                            $err = true;
                            echo '<b>Budget</b>: Please mention the amount required.<br>';
                        }
                        if (isset($_FILES['attach']) && $_FILES['attach']['error'] > 0 && strlen($_FILES['attach']['name']) > 1) {
                            $err = true;
                            echo 'Attachment: File not uploaded properly';
                        } else if ($_FILES['attach']['size'] / (1024 * 1024) > 5) {
                            $err = true;
                            echo 'Attachment: Max. file size 5mb.';
                        }

                        if (!$err) {
                            get('Database')->select(
                                quot(Procedure::$name),
                                array(
                                    Procedure::$field_process
                                ),
                                'WHERE ?=?',
                                array(
                                    Procedure::$field_id,
                                    $_POST['procedure_id']
                                )
                            );
                            $row = get('Database')->row();
                            $fwd = $row[Procedure::$field_process];
                            $fwd = explode(';', $fwd);
                            $fwd = $fwd[0];

                            get('Database')->select(
                                quot(Person::$name),
                                array(
                                    Person::$field_name,
                                    Person::$field_post,
                                    Person::$field_group
                                ),
                                "WHERE ?=?",
                                array(
                                    Person::$field_id,
                                    $fwd
                                )
                            );
                            $filename = '';
                            if (strlen($_FILES['attach']['name']) > 3) {
                                $filename = $_FILES['attach']['name'];
                                print_r($_FILES);
                            }
                            $person = get('Database')->row();
                            if (!is_array($person)) {
                                echo 'Fatal Error: Approving body does not exist. Please report to administrator.';
                                get('Logger')->log('FATAL ERROR: No user with id(' . $fwd . ') exist in database. Delete procedure id(' . $_POST['procedure_id'] . ')\r\n');
                                break;
                            }
                            //Todo save attachment
                            $person = array(
                                array(
                                    'name' => $person[Person::$field_name],
                                    'post' => $person[Person::$field_post] . ', ' . $person[Person::$field_group],
                                    'status' => false
                                )
                            );

                            get('Database')->insert(
                                File::$name,
                                array(
                                    File::$field_subject => $_POST['subject'],
                                    File::$field_requirements => $_POST['requirements'],
                                    File::$field_budget => $_POST['budget'],
                                    File::$field_details => $_POST['details'],
                                    File::$field_filed_by => get('Auth')->logged_id(),
                                    File::$field_procedure_id => $_POST['procedure_id'],
                                    File::$field_forwarded_to => $fwd,
                                    File::$field_completed_up_to => json_encode($person),
                                    File::$field_attach => $filename
                                )
                            );
                            header('Location: ' . get('home_url'));
                        } else {

                        }
                    } else {
                        echo 'some error occurred';
                    }
                } else echo 'You are not authorised for this action';
                break;
            case 'filecheck':
                $user = $this->getPermissions();
                $errstr = '';
                if ($user['all'] || (array_key_exists('home.create.file', $user['perms']) && $user['perms']['home.create.file'])) {
                    $err = false;
                    if (isset($_POST['procedure_id']) && isset($_POST['subject']) && isset($_POST['requirements']) && isset($_POST['details']) && isset($_POST['budget'])) {
                        if (strlen($_POST['subject']) < 5) {
                            $err = true;
                            $errstr .= '<b>Subject</b>: It should be a little more descriptive.<br>';
                        }
                        if (strlen($_POST['requirements']) < 5) {
                            $err = true;
                            $errstr .= '<b>Requirements</b>: A brief description of your requirements is helpful<br>';
                        }
                        if (strlen($_POST['details']) < 16) {
                            $err = true;
                            $errstr .= '<b>Details</b>: Make it a little more detailed.<br>';
                        }
                        if (strlen($_POST['budget']) < 1 || intval($_POST['budget']) < 0) {
                            $err = true;
                            $errstr .= '<b>Budget</b>: Please mention the amount required.<br>';
                        }
                    } else {
                        $errstr .= 'some error occurred';
                    }
                } else $errstr .= 'You are not authorised for this action';
                if ('' == $errstr)
                    $errstr = "+OK";
                echo $errstr;
                break;
        }
    }

    private function getPermissions()
    {
        if (isset($_SESSION['user_permissions'])) {
            return $_SESSION['user_permissions'];
        } else {
            get('Database')->select(
                Person::$name,
                array(Person::$field_power),
                'WHERE `?`=?',
                array(Person::$field_id, intval($_SESSION['xdec_user_id']))
            );
            $user = get('Database')->row();
            $user['perms'] = (array)json_decode($user[Person::$field_power]);
            $perm = (array_key_exists('home.create.*', $user['perms']) && $user['perms']['home.create.*']) || (array_key_exists('home.*', $user['perms']) && $user['perms']['home.*']);
            $del = (array_key_exists('home.delete.*', $user['perms']) && $user['perms']['home.delete.*']) || (array_key_exists('home.*', $user['perms']) && $user['perms']['home.*']);
            $_SESSION['user_permissions'] = array(
                'perms' => $user['perms'],
                'all' => $perm,
                'del' => $del
            );
            return $_SESSION['user_permissions'];
        }
    }

    function approve($var)
    {
        get('Database')->select(
            quot(File::$name),
            '*',
            "WHERE `?`=?",
            array(
                File::$field_id,
                intval($var)
            )
        );
        if (get('Database')->num_rows() == 1) {
            $row = get('Database')->row();
            if ($row[File::$field_forwarded_to] == get('Auth')->logged_id()) {
                get('Database')->select(
                    quot(Procedure::$name),
                    '*',
                    "WHERE `?`=?",
                    array(
                        Procedure::$field_id,
                        $row[File::$field_procedure_id]
                    )
                );
                $newFWD = 0;
                $proc = get('Database')->row();
                $body = explode(';', $proc[Procedure::$field_process]);
                foreach ($body as $key => $value) {
                    if ($value == get('Auth')->logged_id()) {
                        if (++$key < count($body))
                            $newFWD = $body[$key];
                        break;
                    }
                }
                if ($newFWD == 0) {
                    get('Database')->update(
                        quot(File::$name),
                        array(
                            File::$field_forwarded_to => '',
                            File::$field_status => 'approved'
                        ),
                        "WHERE `?`=?",
                        array(
                            File::$field_id,
                            intval($var)
                        )
                    );
                } else {
                    $comp = json_decode($row[File::$field_completed_up_to]);
                    $usr = $this->user_meta();
                    array_push($comp, array(
                        'name' => $usr[Person::$field_name],
                        'post' => $usr[Person::$field_post] . ', ' . $usr[Person::$field_group]
                    ));
                    get('Database')->update(
                        quot(File::$name),
                        array(
                            File::$field_forwarded_to => $newFWD,
                            File::$field_completed_up_to => json_encode($comp)
                        ),
                        "WHERE `?`=?",
                        array(
                            File::$field_id,
                            intval($var)
                        )
                    );
                }
                echo 'Okay!';
            } else echo 'You\'re not authorised for this action.';
        } else echo 'Oops! This data does not exist';
    }

    function reject($var)
    {
        get('Database')->select(
            quot(File::$name),
            '*',
            "WHERE `?`=?",
            array(
                File::$field_id,
                intval($var)
            )
        );
        if (get('Database')->num_rows() == 1) {
            $row = get('Database')->row();
            if ($row[File::$field_forwarded_to] == get('Auth')->logged_id()) {
                get('Database')->update(
                    quot(File::$name),
                    array(
                        File::$field_forwarded_to => '',
                        File::$field_status => 'rejected'
                    ),
                    "WHERE `?`=?",
                    array(
                        File::$field_id,
                        intval($var)
                    )
                );
                echo 'Okay!';
            } else echo 'You\'re not authorised for this action.';
        } else echo 'Oops! This data does not exist';
    }

    function delete($var)
    {
        switch ($var) {
            case 'user':
                $user = $this->getPermissions();
                if ($user['del'] || (array_key_exists('home.delete.user', $user['perms']) && $user['perms']['home.delete.user'])) {
                    if (isset($_GET['id']) && intval($_GET['id']) != 0) {
                        get('Database')->delete(
                            quot(Person::$name),
                            "WHERE `?`=?",
                            array(
                                Person::$field_id,
                                intval($_GET['id'])
                            )
                        );
                        get('Database')->delete(
                            quot(Login::$name),
                            "WHERE `?`=?",
                            array(
                                Login::$field_id,
                                intval($_GET['id'])
                            )
                        );
                        echo 'user deleted :(';
                    } else {
                        echo 'Some error occurred :(';
                    }
                } else echo 'You\'re not authorised for this action';
                break;
            case 'procedure':
                $user = $this->getPermissions();
                if ($user['del'] || (array_key_exists('home.delete.procedure', $user['perms']) && $user['perms']['home.delete.procedure'])) {
                    if (isset($_GET['id']) && intval($_GET['id']) != 0) {
                        get('Database')->delete(
                            quot(Procedure::$name),
                            "WHERE `?`=?",
                            array(
                                Procedure::$field_id,
                                intval($_GET['id'])
                            )
                        );
                        echo 'procedure deleted :(';
                    } else {
                        echo 'Some error occurred :(';
                    }
                } else echo 'You\'re not authorised for this action';
                break;
        }
    }

    private function user_meta()
    {
        if (isset($_SESSION['x_auth_user_meta'])) {
            return $_SESSION['x_auth_user_meta'];
        } else {
            get('Database')->select(
                quot(Person::$name),
                array(
                    Person::$field_name,
                    Person::$field_post,
                    Person::$field_group
                ),
                "WHERE `?`=?",
                array(
                    Person::$field_id,
                    get('Auth')->logged_id()
                )
            );
            $_SESSION['x_auth_user_meta'] = get('Database')->row();
            return $_SESSION['x_auth_user_meta'];
        }
    }

    private function showDelete()
    {
        $user = $this->getPermissions();
        if ($user['del'] || (array_key_exists('home.delete.procedure', $user['perms']) && $user['perms']['home.delete.procedure'])) {
            require_once(CONTENT . 'admin/index.php');
            delete_procedure();
        }
        if ($user['del'] || (array_key_exists('home.delete.user', $user['perms']) && $user['perms']['home.delete.user'])) {
            require_once(CONTENT . 'admin/index.php');
            delete_user();
        }
    }

    function startOutput($var)
    {
        // TODO: Implement startOutput() method.
    }
}