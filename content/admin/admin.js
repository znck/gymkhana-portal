$(document).ready(function () {
    console.log('Hello world');
    $('input[type=checkbox]').on('click', function () {
        $(this).parent().attr('state', $(this).is(':checked') ? 'checked' : 'unchecked');
    });
    $('.asynchronous').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            data: $(this).serialize(),
            url: $(this).attr('action'),
            type: 'post'
        }).done(function (resp) {
                showAlert('', resp, null, '', 'ok');
            });
    });
    $('.element').hide();
    if (window.location.hash)
        $(window.location.hash).show();
    else
        $('#filed_requests').show();
    $('.person.draggable').on('touchstart', drag);
    $('#file').on('submit', request_submit);
});

function drop(ev) {
    var data = ev.dataTransfer.getData("Text");
    var wrapper = document.createElement('div');
    $(wrapper).addClass('node');
    var ele = $('#' + data);
    var user = data.substr(6);
    $('#procedure_process').val($('#procedure_process').val() + user + ';');
    ele.attr({
        'draggable': 'false'
    })
    $('#process_drop_zone').find(':last-child').addClass('fill').append(ele.get()[0]);
    $('#process_drop_zone').append(wrapper);
}

function drag(event) {
    event.dataTransfer.setData("Text", $(event.target).attr('id'));
}

function allowDrop(event) {
    event.preventDefault();
}

function process_clear() {
    $.get(home_url + 'home/list_users', function (data) {
        $('#person_container').html(data);
    });
    $('#process_drop_zone').html('<div class="node"></div>');
    $('#procedure_process').val('');

}

function toggle_menu() {
    $('#main-menu').toggle();
}

function show(v) {
    $('.element').hide();
    $(v).show();
}

function details(id) {
    var $id = $(id);
    if($id.hasClass('small')) $id.removeClass('small'); else $id.addClass('small');
    $id.find('.details').toggle();
}

function reject_req(id) {
    $.ajax(
        {
            url: './reject/' + id,
            method: 'post',
            success: function (e) {
                if (e == 'Okay!')
                    showAlert('', e, function () {
                        window.location.href = window.location.href;
                    }, '', 'ok', 0, false, function () {
                        window.location.href = window.location.href;
                    });
                else
                    showAlert('', e, function () {
                    }, '', 'ok');
            }
        }
    );
}

function approve_req(id) {
    $.ajax(
        {
            url: './approve/' + id,
            method: 'post',
            success: function (e) {
                if (e == 'Okay!')
                    showAlert('', e, function () {
                        window.location.href = window.location.href;
                    }, '', 'ok', 0, false, function () {
                        window.location.href = window.location.href;
                    });
                else
                    showAlert('', e, function () {
                    }, '', 'ok');
            }
        }
    );
}

function showAlert(title, text, action, button2, button1, delay, always, finish) {
    delay = 0 | delay;
    always = always == true;
    action = action ? action : function(){};
    button1 = button1 ? button1 : '';
    button2 = button2 ? button2 : 'ok';
    finish = finish ? finish : function(){};
    var div = document.createElement('div');
    var b1 = document.createElement('div');
    var b2 = document.createElement('div');
    var id = 'alert-box-'+$('#alert > div').length;
    $(b1).addClass('button ok').html(button1).on('click', action);
    $(b2).addClass('button cancel').html(button2).on('click', function () {
        $('#'+id).remove();
        if($('#alert > div').length == 0)
            $('#alert').hide();
        if (finish)
            finish();
    });
    $(div).addClass('center').html('<h1>' + title + '</h1><div>' + text + '</div>').attr('id', id);
    if (button2.length > 0) $(div).append(b2);
    if (button1.length > 0) $(div).append(b1);
    window.setTimeout(function () {
        $('#alert').append(div).show();/*.on('click', function () {
//            if (!always) {
//                $('#'+id).remove();
//                if($('#alert > div').length == 0)
//                    $('#alert').hide();
//            }
//            if (finish)
//                finish();
        });*/
    }, delay);
    return id;
}

function delete_user(id) {
    showAlert('Delete user', 'Are you sure you want to do this?', function () {
        $.get('./delete/user?id=' + id, function (e) {
            showAlert('', e, null, '', 'ok', 0, false, function () {
                window.location.href = window.location.href;
            });
        });
    }, 'Delete', 'Cancel');
}

function delete_procedure(id) {
    showAlert('Delete procedure', 'Are you sure you want to do this?', function () {
        $.get('./delete/procedure?id=' + id, function (e) {
            showAlert('', e, null, '', 'ok', 0, false, function () {
                window.location.href = window.location.href;
            });
        });
    }, 'Delete', 'Cancel');
}

function request_assist() {

}
var bypass = false;
function request_submit(e) {
    if (bypass) {
    } else {
        bypass = false;
        e.preventDefault();
        $.ajax({
            data: $(this).serialize(),
            url: $(this).attr('action') + 'check',
            type: 'post'
        }).done(function (resp) {
                if (resp == "+OK") {
                    bypass = true;
                    this.submit();
                }
                else showAlert('', resp, null, '', 'ok');
            });
    }
}


function preview(formId){

}