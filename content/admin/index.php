<?php
/**
 * Developer: Rahul Kadyan
 * Date: 08/08/13
 * Time: 4:23 PM
 * Product: JetBrains PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
if (!defined('xDEC')) exit;
function create_user()
{
    ?>
    <div id="user_form_container" class="element">
        <h1>Create new user</h1>

        <div class="form">
            <form id="user" class="asynchronous" action="<?php echo get('home_url'); ?>home/form/user">
                <input form="user" type="text" name="username" placeholder="Username">
                <input form="user" type="text" name="name" placeholder="Full name">
                <input form="user" type="text" name="post" placeholder="Position/Designation">
                <input form="user" type="text" name="group" placeholder="Board/Reference">
                <input form="user" type="text" name="password" placeholder="Password">
                <label class="checkbox block perm_class">
                    <input type="checkbox" form="user" name="home_*" id="perm_home_all" class="perm" parent="none"> All
                </label>
                <label class="checkbox block perm_function">
                    <input type="checkbox" form="user" name="home_create_*" id="perm_home_create_all" class="perm"
                           parent="perm_home_all"> Create
                </label>
                <label class="checkbox block perm_action">
                    <input type="checkbox" form="user" name="home_create_user" id="perm_home_create_user" class="perm"
                           parent="perm_home_create_all"> User
                </label>
                <label class="checkbox block perm_action">
                    <input type="checkbox" form="user" name="home_create_procedure" id="perm_home_create_procedure"
                           class="perm" parent="perm_home_create_all"> Procedure
                </label>
                <label class="checkbox block perm_action">
                    <input type="checkbox" form="user" name="home_create_file" id="perm_home_create_file" class="perm"
                           parent="perm_home_create_all"> Request
                </label>
                <label class="checkbox block perm_function">
                    <input type="checkbox" form="user" name="home_delete_*" id="perm_home_delete_all" class="perm"
                           parent="perm_home_all"> Delete
                </label>
                <label class="checkbox block perm_action">
                    <input type="checkbox" form="user" name="home_delete_user" id="perm_home_delete_user" class="perm"
                           parent="perm_home_delete_all"> User
                </label>
                <label class="checkbox block perm_action">
                    <input type="checkbox" form="user" name="home_delete_procedure" id="perm_home_delete_procedure"
                           class="perm" parent="perm_home_delete_all"> Procedure
                </label>
                <input form="user" type="submit">
            </form>
        </div>
    </div>
    <script>
        $(document).ready(
            function () {
                $('#main-menu').append('<span class="item" onclick="show(\'#user_form_container\')">Create new user</span> ')
            }
        )
    </script>
<?php
}

function login_form()
{
    ?>
    <div style="width: 1000px; margin: 0 auto">
    <h1 style="display: block; color: #444">
        <img src="<?php echo static_url('content/images/gymkahana-logo.png'); ?>" style="float: left; margin: 16px">
        Gymkhana <br>
        Permissions Portal
    </h1>
    <div id="user_form_container" class="form" style="display: inline-block; float: left">
        <form id="login" action="<?php echo get('home_url'); ?>home/login" method="post">
            <input form="login" type="text" name="username" placeholder="Username">
            <input form="login" type="password" name="password" placeholder="Password">
            <input form="login" type="submit" value="Submit">
        </form>
    </div>
    <div style="display: inline-block; border-left: 1px solid #999; padding-left: 16px">
    <div style="font-size: 36px; color: #333; max-width: 600px">Now permissions and approvals got easy. Just three easy
        steps
        <div>
            <div class="li">
                <div class="circle">1</div>
                <div class="content">Create a request</div>
            </div>
            <div class="li">
                <div class="circle">2</div>
                <div class="content">Wait for approval</div>
            </div>
            <div class="li">
                <div class="circle">3</div>
                <div class="content">Print approved permission</div>
            </div>
        </div>
    </div>
<?php
}

function create_procedure()
{
    ?>
    <div id="procedure_form_container" class="form">
        <form id="procedure" class="asynchronous" action="<?php echo get('home_url'); ?>home/form/procedure">
            <input form="procedure" type="text" name="title" placeholder="Title">
            <input form="procedure" type="text" name="group" placeholder="Serial ID">
            <input form="procedure" type="text" name="individuals" class="hidden-input" placeholder="Individuals">
            <input form="procedure" type="text" name="process" id="procedure_process" class="hidden-input"
                   placeholder="Process">

            <div id="process_drop_zone" dropzone="true" ondrop="drop(event)" ondragover="allowDrop(event)">

                <div class="node"></div>
            </div>
            <div id="clear" onclick="process_clear();" style="cursor: pointer">Clear</div>
            <label class="checkbox"><input form="procedure" type="checkbox" name="status">Enable</label>
            <input form="procedure" type="submit" value="Submit">
        </form>
    </div>
<?php
}

function create_group()
{
    /*
?>
    <div id="group_form_container" class="form">
        <form id="group" class="asynchronous" action="<?php echo get('home_url'); ?>home/form/group">
            <input form="group" type="text" name="title" placeholder="Group name">
            <input form="group" type="text" name="members" class="hidden-input" placeholder="Members">
            <textarea form="group" type="text" name="details" placeholder="Details about this group" style="resize: none;"></textarea>
            <input form="group" type="submit">
        </form>
    </div>
<?php */
}

function create_file()
{
    get('Database')->select(
        quot(Procedure::$name),
        '*',
        'ORDER BY ? ASC',
        array(
            Procedure::$field_title
        )
    )
    ?>
    <div id="file_form_container" class="element">
        <h1>Create new request</h1>

        <div class="form" style="width: 300px; display: inline-block;float: left">
            <form id="file" class="" action="<?php echo get('home_url'); ?>home/form/file" enctype="multipart/form-data"
                  method="post">
                <select form="file" type="" name="procedure_id" placeholder="Procedure ID">
                    <?php
                    while ($row = get('Database')->row()) {
                        echo '<option value="' . $row[Procedure::$field_id] . '">' . $row[Procedure::$field_title] . '</option>';
                    }
                    ?>
                </select>
                <input form="file" type="text" name="subject" placeholder="Subject">
                <input form="file" type="text" name="requirements" placeholder="Venue">
                <input form="file" type="text" name="budget" placeholder="Budget">
                <textarea form="file" type="text" name="details" placeholder="Details about this file"></textarea>
                <!--            <label class="checkbox"><input form="file" type="checkbox" name="visibility"> Draft</label>-->
                <input form="file" type="file" name="attach" id="attach" accept="application/pdf"
                       placeholder="Attach file">
                <input form="file" type="submit" value="Submit">
            </form>
        </div>

        <div id="preview" style="display: inline-block; max-width: 700px">
            <?php
            //form('gymkhana');
            ?>
        </div>
    </div>
    <script>
        $(document).ready(
            function () {
                $('#main-menu').append('<span class="item" onclick="show(\'#file_form_container\')">Create new request</span>')
            }
        )
    </script>
<?php
}

function list_all_users()
{

    get('Database')->select(
        Person::$name,
        array(
            Person::$field_id,
            Person::$field_name,
            Person::$field_post,
            Person::$field_group
        ),
        '',
        null
    );
    while ($row = get('Database')->row()) {
        echo '<div id="person' . $row[Person::$field_id] . '" class="person draggable" draggable="true" ondragstart="drag(event)"><div class="name">' . $row[Person::$field_name] . '</div><div class="post">' . $row[Person::$field_post] . ', ' . $row[Person::$field_group] . '</div></div>';
    }
}

function list_all_groups()
{
    ?>
    <div>
        <?php
        get('Database')->select(
            Meta::$name,
            array(
                Meta::$field_id,
                Meta::$field_tag,
                Meta::$field_value
            ),
            'WHERE `?` = \'\'',
            null
        );
        while ($row = get('Database')->row()) {
            echo '<div class="group" ><div >' . $row[Person::$field_name] . '</div><div>' . $row[Person::$field_post] . '</div></div>';
        }
        ?>
    </div>
<?php
}

function list_my_files()
{
    ?>
    <div id="filed_requests" class="files element">
        <h1>My requests</h1>
        <?php
        get('Database')->select(
            quot(File::$name),
            '*',
            "WHERE `?`=? ORDER BY `?`",
            array(
                File::$field_filed_by,
                get('Auth')->logged_id(),
                File::$field_status
            )
        );
        if (get('Database')->num_rows() == 0) {
            ?>
            <h2>No requests filed. :)</h2>
        <?php
        }
        while ($row = get('Database')->row()) {
            ?>
            <div id="file_<?php echo $row[File::$field_id]; ?>" class="file <?php echo $row[File::$field_status]; ?>">
                <div class="dateTime"><?php $d = new DateTime($row[File::$field_timestamp]);
                    echo $d->format('M d, h:ia'); ?></div>
                <h3><?php echo $row[File::$field_subject]; ?></h3>

                <div class="venue">Venue: <span><?php echo $row[File::$field_requirements]; ?></span></div>
                <div class="budget">Budget: <span>&#8377;<?php echo $row[File::$field_budget]; ?></span></div>
                <br>

                <div class="button detail" onclick="details('#file_<?php echo $row[File::$field_id]; ?>')">...</div>
                <div class="details">
                    <h4>Details:</h4>

                    <div class="content">
                        <?php echo nl2br($row[File::$field_details]); ?>
                    </div>
                    <div class="status">
                        <?php
                        $d = json_decode($row[File::$field_subject]);
                        if (is_array($d))
                            foreach ($d as $p) {
                                ?>
                                <div
                                    class="node <?php if ($p['status']) echo 'ok'; ?>"><?php echo $p['name'] . ', ' . $p['post'] ?></div>
                                <?php if (!$p['status']) { ?>
                                    <div class="sep">&raquo;</div>
                                <?php
                                }
                            }
                        ?>
                    </div>
                    <div class="button detail" onclick="details('#file_<?php echo $row[File::$field_id]; ?>')">...</div>
                    <?php if ($row[File::$field_attach] != '') { ?>
                        <div class="attach button">
                            <a href="<?php echo get('static_url') . 'content/uploads/' . $row[File::$field_id] . '.pdf' ?>">Attached
                                file</a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <br>
        <?php
        }
        ?>
    </div>
    <script>
        $(document).ready(
            function () {
                $('#main-menu').append('<span class="item" onclick="show(\'#filed_requests\')">View Status</span> ')
            }
        )
    </script>
<?php
}

function list_my_requests()
{
    ?>
    <div id="rec_requests" class="files element">
        <h1>Review request</h1>
        <?php
        get('Database')->select(
            quot(File::$name),
            '*',
            "WHERE `?`=? ORDER BY `?`",
            array(
                File::$field_forwarded_to,
                get('Auth')->logged_id(),
                File::$field_timestamp
            )
        );
        if (get('Database')->num_rows() == 0) {
            ?>
            <h2>Nothing to review. :)</h2>
        <?php
        }
        while ($row = get('Database')->row()) {
            ?>
            <div id="req_<?php echo $row[File::$field_id]; ?>" class="file <?php echo $row[File::$field_status]; ?>">
                <div class="dateTime"><?php $d = new DateTime($row[File::$field_timestamp]);
                    echo $d->format('M d, h:ia'); ?></div>
                <h3><?php echo $row[File::$field_subject]; ?></h3>

                <div class="venue">Venue: <span><?php echo $row[File::$field_requirements]; ?></span></div>
                <div class="budget">Budget: <span>&#8377;<?php echo $row[File::$field_budget]; ?></span></div>
                <br>

                <div class="button detail" onclick="details('#req_<?php echo $row[File::$field_id]; ?>')">...</div>
                <div class="details">
                    <h4>Details:</h4>

                    <div class="content">
                        <?php echo nl2br($row[File::$field_details]); ?>
                    </div>
                    <div class="status">
                        <?php
                        $d = json_decode($row[File::$field_forwarded_to]);
                        if (is_array($d))
                            foreach ($d as $p) {
                                ?>
                                <div
                                    class="node <?php if ($p['status']) echo 'ok'; ?>"><?php echo $p['name'] . ', ' . $p['post'] ?></div>
                                <?php if (!$p['status']) { ?>
                                    <div class="sep">&raquo;</div>
                                <?php
                                }
                            }
                        ?>
                    </div>
                    <div class="button detail" onclick="details('#req_<?php echo $row[File::$field_id]; ?>')">...</div>
                    <div class="button reject" onclick="reject_req('<?php echo $row[File::$field_id]; ?>')">Reject</div>
                    <div class="button approve" onclick="approve_req('<?php echo $row[File::$field_id]; ?>')">Approve
                    </div>
                    <?php if ($row[File::$field_attach] != '') { ?>
                        <div class="attach button">
                            <a href="<?php echo get('static_url') . 'content/uploads/' . $row[File::$field_id] . '.pdf' ?>">Attached
                                file</a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <br>
        <?php
        }
        ?>
    </div>
    <script>
        $(document).ready(
            function () {
                $('#main-menu').append('<span class="item" onclick="show(\'#rec_requests\')">Review requests</span> ')
            }
        )
    </script>
<?php
}

function list_my_archives()
{

}

function list_my_history()
{

}

function list_all_requests()
{
    ?>
    <div id="all_requests" class="files element">
        <h1>All requests</h1>
        <?php
        get('Database')->select(
            quot(File::$name),
            '*',
            "ORDER BY `?`",
            array(
                File::$field_timestamp
            )
        );
        if (get('Database')->num_rows() == 0) {
            ?>
            <h2>No requests filed. :)</h2>
        <?php
        }
        while ($row = get('Database')->row()) {
            ?>
            <div id="all_<?php echo $row[File::$field_id]; ?>" class="file <?php echo $row[File::$field_status]; ?>">
                <div class="dateTime"><?php $d = new DateTime($row[File::$field_timestamp]);
                    echo $d->format('M d, h:ia'); ?></div>
                <h3><?php echo $row[File::$field_subject]; ?></h3>

                <div class="venue">Venue: <span><?php echo $row[File::$field_requirements]; ?></span></div>
                <div class="budget">Budget: <span>&#8377;<?php echo $row[File::$field_budget]; ?></span></div>
                <br>

                <div class="button detail" onclick="details('#file_<?php echo $row[File::$field_id]; ?>')">...</div>
                <div class="details">
                    <h4>Details:</h4>

                    <div class="content">
                        <?php echo nl2br($row[File::$field_details]); ?>
                    </div>
                    <div class="status">
                        <?php
                        $d = json_decode($row[File::$field_completed_up_to]);
                        if (is_array($d))
                            foreach ($d as $p) {
                                ?>
                                <div
                                    class="node <?php if ($p['status']) echo 'ok'; ?>"><?php echo $p['name'] . ', ' . $p['post'] ?></div>
                                <?php if (!$p['status']) { ?>
                                    <div class="sep">&raquo;</div>
                                <?php
                                }
                            }
                        ?>
                    </div>
                    <div class="button detail" onclick="details('#all_<?php echo $row[File::$field_id]; ?>')">...</div>
                    <?php if ($row[File::$field_attach] != '') { ?>
                        <div class="attach button">
                            <a href="<?php echo get('static_url') . 'content/uploads/' . $row[File::$field_id] . '.pdf' ?>">Attached
                                file</a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <br>
        <?php
        }
        ?>
    </div>
    <script>
        $(document).ready(
            function () {
                $('#main-menu').append('<span class="item" onclick="show(\'#all_requests\')">All requests</span> ');
            }
        )
    </script>
<?php
}

function delete_user()
{
    ?>
    <div id="delete-user" class="element delete">
        <h1>Delete Users</h1>
        <?php
        get('Database')->select(
            Person::$name,
            array(
                Person::$field_id,
                Person::$field_name,
                Person::$field_post,
                Person::$field_group
            ),
            '',
            null
        );

        while ($row = get('Database')->row()) {
            echo '<div onclick="delete_user(' . $row[Person::$field_id] . ')" id="person' . $row[Person::$field_id] . '" class="person"><div class="name">' . $row[Person::$field_name] . '</div><div class="post">' . $row[Person::$field_post] . ', ' . $row[Person::$field_group] . '</div></div>';
        }
        ?>
    </div>
    <script>
        $(document).ready(
            function () {
                $('#main-menu').append('<span class="item" onclick="show(\'#delete-user\')">Delete user</span> ')
            }
        )
    </script>
<?php
}

function delete_procedure()
{
    ?>
    <div id="delete-procedure" class="element delete">
        <h1>Delete Procedure</h1>
        <?php
        get('Database')->select(
            quot(Procedure::$name),
            array(
                Procedure::$field_id,
                Procedure::$field_title
            ),
            '',
            null
        );

        while ($row = get('Database')->row()) {
            echo '<div onclick="delete_procedure(' . $row[Procedure::$field_id] . ')" id="person' . $row[Procedure::$field_id] . '" class="person"><div class="name">' . $row[Procedure::$field_title] . '</div></div>';
        }
        ?>
    </div>
    <script>
        $(document).ready(
            function () {
                $('#main-menu').append('<span class="item" onclick="show(\'#delete-procedure\')">Delete procedure</span> ')
            }
        )
    </script>
<?php
}
