<?php
/**
 * Developer: Rahul Kadyan
 * Date: 05/01/14
 * Time: 11:01 PM
 * Product: PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
function urlBinding($segments)
{
    get('Logger')->log($segments);
    $binds = array(
        '' =>
            array('folder' => 'page', 'file' => 'index'),
        '\/login' =>
            array('folder' => 'page/controllers', 'file' => 'login'),
        '\/logout' =>
            array('folder' => 'page/controllers', 'file' => 'logout'),
        '\/create\/user' =>
            array('folder' => 'page/create', 'file' => 'user'),
        '\/create\/procedure' =>
            array('folder' => 'page/create', 'file' => 'procedure'),
        '\/create\/proposal' =>
            array('folder' => 'page/create', 'file' =>  'proposal'),
        '\/create\/advance' =>
            array('folder' => 'page/create', 'file' => 'advance'),
        '\/create\/refund' =>
            array('folder' => 'page/create', 'file' => 'refund'),
        '\/delete\/user' =>
            array('folder' => 'page/delete', 'file' => 'user'),
        '\/delete\/procedure' =>
            array('folder' => 'page/delete', 'file' => 'procedure')
    );
    foreach ($binds as $key => $val) {
        if (preg_match('/^'.$key.'\/?$/', $segments, $matches)) {
            set(BINDING_MATCHES, $matches);
            set(ROUTER_FOLDER, $binds[$key]['folder']);
            set(ROUTER_PAGE, $binds[$key]['file']);
            if (isset($binds[$key]['cache'])) set(CACHED, true);
            else if (isset($binds[$key]['pre-cache']) && strtolower($_SERVER['REQUEST_METHOD']) == 'get') {
                set(CACHED, true);
            } else set(CACHED, false);
            $req_url = explode("?", get('REQUEST_URI'));
            set(CACHE_FILE, md5($binds[$key]['folder'] . $binds[$key]['file'] . $req_url[0]));
            return true;
        }
    }
    return false;
}