<?php
/**
 * Developer: Rahul Kadyan
 * Date: 05/01/14
 * Time: 10:44 PM
 * Product: PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */

class index extends Admin{
    function __head__($var)
    {
        parent::__head__($var);
        require_once('theme.css.snippet.php');
    }

    function __title__($var)
    {
        parent::__title__($var);
    }

    function __body__($var)
    {
        parent::__body__($var);
        @require_once('user.inc.php');
        @require_once(CONTENT.'page/controllers/created.inc.php');
        @require_once(CONTENT.'page/controllers/approvals.inc.php');
        @require_once(CONTENT.'page/controllers/actions.inc.php');
    }

    function end_body()
    {
        parent::end_body();
    }

    function startOutput($var)
    {
        $this->__head__('');
        $this->__title__(' | Home');
        $this->__body__('');
        $this->end_body();
    }
}

set(PAGE_OBJECT, new index());