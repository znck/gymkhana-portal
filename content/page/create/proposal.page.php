<?php
/**
 * Developer: Rahul Kadyan
 * Date: 06/01/14
 * Time: 6:19 PM
 * Product: PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
if(!defined('xDEC')) exit;
class form_proposal extends Admin
{
    function startOutput($var)
    {
        @require_once(CONTENT.'page/controllers/functions.php');
        // TODO form processing @Anuj
        $user = get_user_permissions();
        if ($user['all'] || (array_key_exists('home.create.file', $user['perms']) && $user['perms']['home.create.file'])) {
            $err = false;
            if (isset($_POST['procedure_id']) && isset($_POST['subject']) && isset($_POST['requirements']) && isset($_POST['details']) && isset($_POST['budget'])) {
                if (strlen($_POST['subject']) < 5) {
                    $err = true;
                    echo '<b>Subject</b>: It should be a little more descriptive.<br>';
                }
                if (strlen($_POST['requirements']) < 5) {
                    $err = true;
                    echo '<b>Requirements</b>: A brief description of your requirements is helpful<br>';
                }
                if (strlen($_POST['details']) < 16) {
                    $err = true;
                    echo '<b>Details</b>: Make it a little more detailed.<br>';
                }
                if (strlen($_POST['budget']) < 1 || intval($_POST['budget']) < 0) {
                    $err = true;
                    echo '<b>Budget</b>: Please mention the amount required.<br>';
                }
                if (isset($_FILES['attach']) && $_FILES['attach']['error'] > 0 && strlen($_FILES['attach']['name']) > 1) {
                    $err = true;
                    echo 'Attachment: File not uploaded properly';
                } else if ($_FILES['attach']['size'] / (1024 * 1024) > 5) {
                    $err = true;
                    echo 'Attachment: Max. file size 5mb.';
                }

                if (!$err) {
                    get('Database')->select(
                        quot(Procedure::$name),
                        array(
                            Procedure::$field_process
                        ),
                        'WHERE ?=?',
                        array(
                            Procedure::$field_id,
                            $_POST['procedure_id']
                        )
                    );
                    $row = get('Database')->row();
                    $fwd = $row[Procedure::$field_process];
                    $fwd = explode(';', $fwd);
                    $fwd = $fwd[0];

                    get('Database')->select(
                        quot(Person::$name),
                        array(
                            Person::$field_name,
                            Person::$field_post,
                            Person::$field_group
                        ),
                        "WHERE ?=?",
                        array(
                            Person::$field_id,
                            $fwd
                        )
                    );
                    $filename = '';
                    if (strlen($_FILES['attach']['name']) > 3) {
                        $filename = $_FILES['attach']['name'];
                        print_r($_FILES);
                    }
                    $person = get('Database')->row();
                    if (!is_array($person)) {
                        echo 'Fatal Error: Approving body does not exist. Please report to administrator.';
                        get('Logger')->log('FATAL ERROR: No user with id(' . $fwd . ') exist in database. Delete procedure id(' . $_POST['procedure_id'] . ')\r\n');
                        return;
                    }
                    //Todo save attachment
                    $person = array(
                        array(
                            'name' => $person[Person::$field_name],
                            'post' => $person[Person::$field_post] . ', ' . $person[Person::$field_group],
                            'status' => false
                        )
                    );

                    get('Database')->insert(
                        File::$name,
                        array(
                            File::$field_subject => $_POST['subject'],
                            File::$field_requirements => $_POST['requirements'],
                            File::$field_budget => $_POST['budget'],
                            File::$field_details => $_POST['details'],
                            File::$field_filed_by => $_SESSION[USER_ID],
                            File::$field_procedure_id => $_POST['procedure_id'],
                            File::$field_forwarded_to => $fwd,
                            File::$field_completed_up_to => json_encode($person),
                            File::$field_attach => $filename
                        )
                    );
                    header('Location: http://' . DOMAIN_NAME .'/');
                } else {

                }
            } else {
                echo 'some error occurred';
                echo print_r($_POST);
            }
        } else echo 'You are not authorised for this action';
    }
}

set(PAGE_OBJECT, new form_proposal());