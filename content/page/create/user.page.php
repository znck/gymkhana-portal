<?php
/**
 * Developer: Rahul Kadyan
 * Date: 20/01/14
 * Time: 5:10 PM
 * Product: PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */

class form_user extends Pages{

    function startOutput($var)
    {
        @require_once(CONTENT.'page/controllers/functions.php');
        $user = get_user_permissions();
        if ($user['all'] || (array_key_exists('home.create.user', $user['perms']) && $user['perms']['home.create.user'])) {
            if (isset($_POST['username']) &&
                isset($_POST['name']) &&
                isset($_POST['password']) &&
                isset($_POST['group']) &&
                isset($_POST['post'])
            ) {
                if(preg_match('^[a-zA-Z0-9_-\.]{3,20}$', $_POST['username']) &&
                   preg_match('^.{3,20}$', $_POST['password']) &&
                   strlen($_POST['name']) > 2 &&
                   strlen($_POST['group']) > 2 &&
                   strlen($_POST['post']) > 2) {
                    echo 'INVALID entries';
                    return;
                }
                $perm_str = array();
                if (isset($_POST['home_*'])) $perm_str['home.*'] = true;
                if (isset($_POST['home_create_*'])) $perm_str['home.create.*'] = true;
                if (isset($_POST['home_create_file'])) $perm_str['home.create.file'] = true;
                if (isset($_POST['home_create_user'])) $perm_str['home.create.user'] = true;
                if (isset($_POST['home_create_procedure'])) $perm_str['home.create.procedure'] = true;
                if(count($perm_str) == 0) $perm_str['home.create.file'] = true;
                get('Database')->select(
                    Login::$name,
                    array(
                        Login::$field_user
                    ),
                    "WHERE ".quot(Login::$field_user)."=?",
                    array(
                        $_POST['username']
                    )
                );
                if (get('Database')->num_rows() > 0) {
                    echo 'Username not available.';
                } else {
                    get('Database')->insert(
                        Login::$name,
                        array(
                            Login::$field_user => $_POST['username'],
                            Login::$field_pass => sha1($_POST['password'])
                        )
                    );
                    $id = get('Database')->insertId();
                    get('Database')->insert(
                        Person::$name,
                        array(
                            Person::$field_name => $_POST['name'],
                            Person::$field_post => $_POST['post'],
                            Person::$field_power => json_encode($perm_str),
                            Person::$field_id => $id,
                            Person::$field_group => $_POST['group']
                        )
                    );
                    echo 'User added :)';
                }
            } else echo 'All fields are required. Try again!!!'.print_r($_POST, true);
        } else echo 'You are not authorised for this action';
    }
}

set(PAGE_OBJECT, new form_user());