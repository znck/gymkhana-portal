<?php
/**
 * Developer: Rahul Kadyan
 * Date: 05/01/14
 * Time: 10:49 PM
 * Product: PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
?>
<div class="page">
    <div id="create" class="create">
        <a class="request" href="./create/advance"><div class="request" id="proposal">
            Advance<br>Form
        </div></a>
        <a class="request" href="./create/settlement"><div class="request" id="settlement">
            Settlement<br>Form
        </div></a>
        <a class="request" href="./create/refund"><div class="request" id="refund">
            Reimbursement<br>Form
        </div></a>
    </div>
    <div id="review" class="review">
        <div class="incomplete" id="approvals"></div>
        <div class="incomplete" id="pending"></div>
    </div>
    <div id="history" class="history"></div>
    <div id="settings" class="settings"></div>
</div>