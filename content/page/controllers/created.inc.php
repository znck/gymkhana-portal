<?php
/**
 * Developer: Rahul Kadyan
 * Date: 20/01/14
 * Time: 2:25 PM
 * Product: PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
if(!defined('xDEC')) exit;

if(get('Auth')->logged()) {
    ?>
    <div id="filed_requests" class="files element">
        <h1>My requests</h1>
        <?php
        get('Database')->select(
            quot(File::$name),
            '*',
            "WHERE ".quot(File::$field_filed_by)."=? ORDER BY ".quot(File::$field_status),
            array(
                $_SESSION[USER_ID]
            )
        );
        if (get('Database')->num_rows() == 0) {
            ?>
            <h2>No requests filed. :)</h2>
        <?php
        }
        while ($row = get('Database')->row()) {
            ?>
            <div id="file_<?php echo $row[File::$field_id]; ?>" class="file <?php echo $row[File::$field_status]; ?> small">
                <div class="dateTime"><?php $d = new DateTime($row[File::$field_timestamp]);
                    echo $d->format('M d, h:ia'); ?></div>
                <h3><?php echo $row[File::$field_subject]; ?></h3>

                <div class="venue">Venue: <span><?php echo $row[File::$field_requirements]; ?></span></div>
                <div class="budget">Budget: <span>&#8377;<?php echo $row[File::$field_budget]; ?></span></div>
                <br>

                <div class="button detail" onclick="details('#file_<?php echo $row[File::$field_id]; ?>')">...</div>
                <div class="details">
                    <h4>Details:</h4>

                    <div class="content">
                        <?php echo nl2br($row[File::$field_details]); ?>
                    </div>
                    <div class="status">
                        <?php
                        $d = json_decode($row[File::$field_subject]);
                        if (is_array($d))
                            foreach ($d as $p) {
                                ?>
                                <div
                                    class="node <?php if ($p['status']) echo 'ok'; ?>"><?php echo $p['name'] . ', ' . $p['post'] ?></div>
                                <?php if (!$p['status']) { ?>
                                    <div class="sep">&raquo;</div>
                                <?php
                                }
                            }
                        ?>
                    </div>
                    <div class="button detail" onclick="details('#file_<?php echo $row[File::$field_id]; ?>')">...</div>
                    <?php if ($row[File::$field_attach] != '') { ?>
                        <div class="attach button">
                            <a href="<?php echo get('static_url') . 'content/uploads/' . $row[File::$field_id] . '.pdf' ?>">Attached
                                file</a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
    <script>
        $(document).ready(
            function () {
                $('#main-menu').append('<span class="item" onclick="show(\'#filed_requests\')">View Status</span> ')
            }
        )
    </script>
<?php
}