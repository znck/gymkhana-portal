<?php
/**
 * Developer: Rahul Kadyan
 * Date: 19/01/14
 * Time: 6:25 PM
 * Product: PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */

class page_login extends Pages {

    function __head__($var)
    {
        parent::__head__($var);
        require_once(CONTENT.'page/theme.css.snippet.php');
    }

    function __title__($var)
    {
        parent::__title__($var);
    }

    function __body__($var)
    {
        parent::__body__('');
        ?>
        <div style="width: 1000px; margin: 0 auto">
        <h1 style="display: block; color: #444">
            <img src="<?php echo get('static_url').('content/images/gymkahana-logo.png'); ?>" style="float: left; margin: 16px">
            Gymkhana <br>
            Permissions Portal
        </h1>
        <div id="user_form_container" class="form" style="display: inline-block; float: left">
            <form id="login" action="<?php echo get('home_url'); ?>login" method="post">
                <input form="login" type="text" name="username" placeholder="Username" required>
                <input form="login" type="password" name="password" placeholder="Password" required>
                <input form="login" type="submit" value="Submit">
                <div style="error"><?php echo $var; ?></div>
            </form>
        </div>
        <div style="display: inline-block; border-left: 1px solid #999; padding-left: 16px">
            <div style="font-size: 36px; color: #333; max-width: 600px">Now permissions and approvals got easy. Just three easy
                steps
                <div>
                    <div class="li">
                        <div class="circle">1</div>
                        <div class="content">Create a request</div>
                    </div>
                    <div class="li">
                        <div class="circle">2</div>
                        <div class="content">Wait for approval</div>
                    </div>
                    <div class="li">
                        <div class="circle">3</div>
                        <div class="content">Print approved permission</div>
                    </div>
                </div>
            </div>
        <?php
    }

    function end_body()
    {
        parent::end_body();
    }

    function startOutput($var)
    {
        $error = '';
        if(isset($_SERVER['REQUEST_METHOD']) && strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
            if(isset($_POST['username']) && isset($_POST['password'])) {
                if(get('Auth')->login($_POST['username'], $_POST['password']))
                    header('Location: http://'.DOMAIN_NAME.'/');
            }
            $error .= 'Invalid Username or Password';
        }
        $this->__head__('');
        $this->__title__(' | Login');
        $this->__body__($error);
        $this->end_body();
    }
}

set(PAGE_OBJECT, new page_login());