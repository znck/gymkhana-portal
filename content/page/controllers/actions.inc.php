<?php
/**
 * Developer: Rahul Kadyan
 * Date: 20/01/14
 * Time: 2:43 PM
 * Product: PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
if(!defined('xDEC')) exit;
if(get('Auth')->logged()) {
    @require_once(CONTENT.'page/controllers/functions.php');
    $user = get_user_permissions();
    // 1. Create
    // 1.1 Create User
    if ($user['all'] || (array_key_exists('home.create.user', $user['perms']) && $user['perms']['home.create.user'])) {
        ?>
        <div id="user_form_container" class="element">
            <h1>Create new user</h1>

            <div class="form">
                <form id="user" class="asynchronous" action="./create/user">
                    <input form="user" type="text" name="username" placeholder="Username">
                    <input form="user" type="text" name="name" placeholder="Full name">
                    <input form="user" type="text" name="post" placeholder="Position/Designation">
                    <input form="user" type="text" name="group" placeholder="Board/Reference">
                    <input form="user" type="text" name="password" placeholder="Password">
                    <label class="checkbox block perm_class">
                        <input type="checkbox" form="user" name="home_*" id="perm_home_all" class="perm" parent="none"> All
                    </label>
                    <label class="checkbox block perm_function">
                        <input type="checkbox" form="user" name="home_create_*" id="perm_home_create_all" class="perm"
                               parent="perm_home_all"> Create
                    </label>
                    <label class="checkbox block perm_action">
                        <input type="checkbox" form="user" name="home_create_user" id="perm_home_create_user" class="perm"
                               parent="perm_home_create_all"> User
                    </label>
                    <label class="checkbox block perm_action">
                        <input type="checkbox" form="user" name="home_create_procedure" id="perm_home_create_procedure"
                               class="perm" parent="perm_home_create_all"> Procedure
                    </label>
                    <label class="checkbox block perm_action">
                        <input type="checkbox" form="user" name="home_create_file" id="perm_home_create_file" class="perm"
                               parent="perm_home_create_all"> Request
                    </label>
                    <label class="checkbox block perm_function">
                        <input type="checkbox" form="user" name="home_delete_*" id="perm_home_delete_all" class="perm"
                               parent="perm_home_all"> Delete
                    </label>
                    <label class="checkbox block perm_action">
                        <input type="checkbox" form="user" name="home_delete_user" id="perm_home_delete_user" class="perm"
                               parent="perm_home_delete_all"> User
                    </label>
                    <label class="checkbox block perm_action">
                        <input type="checkbox" form="user" name="home_delete_procedure" id="perm_home_delete_procedure"
                               class="perm" parent="perm_home_delete_all"> Procedure
                    </label>
                    <input form="user" type="submit">
                </form>
            </div>
        </div>
        <script>
            $(document).ready(
                function () {
                    $('#main-menu').append('<span class="item" onclick="show(\'#user_form_container\')">Create new user</span> ')
                }
            )
        </script>
    <?php
    }
    // 1.2 Create Procedure
    if ($user['all'] || (array_key_exists('home.create.procedure', $user['perms']) && $user['perms']['home.create.procedure'])) {
        ?>
        <div id="create_proc" class="element">
            <h1>Create new approving procedure</h1>

            <div class="border">
                <div id="procedure_form_container" class="form">
                    <form id="procedure" class="asynchronous" action="./create/procedure">
                        <input form="procedure" type="text" name="title" placeholder="Title">
                        <input form="procedure" type="text" name="group" placeholder="Serial ID">
                        <input form="procedure" type="text" name="individuals" class="hidden-input" placeholder="Individuals">
                        <input form="procedure" type="text" name="process" id="procedure_process" class="hidden-input"
                               placeholder="Process">

                        <div id="process_drop_zone" dropzone="true" ondrop="drop(event)" ondragover="allowDrop(event)">

                            <div class="node"></div>
                        </div>
                        <div id="clear" onclick="process_clear();" style="cursor: pointer">Clear</div>
                        <label class="checkbox"><input form="procedure" type="checkbox" name="status">Enable</label>
                        <input form="procedure" type="submit" value="Submit">
                    </form>
                </div>
                <div id="person_container">
                    <?php
                    get('Database')->select(
                        Person::$name,
                        array(
                            Person::$field_id,
                            Person::$field_name,
                            Person::$field_post,
                            Person::$field_group
                        ),
                        '',
                        array()
                    );
                    while ($row = get('Database')->row()) {
                        echo '<div id="person' . $row[Person::$field_id] . '" class="person draggable" draggable="true" ondragstart="drag(event)"><div class="name">' . $row[Person::$field_name] . '</div><div class="post">' . $row[Person::$field_post] . ', ' . $row[Person::$field_group] . '</div></div>';
                    }
                    ?>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(
                function () {
                    $('#main-menu').append('<span class="item" onclick="show(\'#create_proc\')">Create new approving procedure</span> ')
                }
            )
        </script>
    <?php
    }
    // TODO change as specified @Anuj
    // 1.3 Create Request
    if ($user['all'] || (array_key_exists('home.create.file', $user['perms']) && $user['perms']['home.create.file'])) {
        get('Database')->select(
            quot(Procedure::$name),
            '*',
            'ORDER BY '.quot(Procedure::$field_title).' ASC',
            array()
        )
        ?>
        <div id="file_form_container" class="element">
            <h1>Create new request</h1>
            <!-- 1.1 Form Proposals -->
            <div id="forms">
                <div class="form-container">
                    <div class="form-title">
                        New Proposal
                    </div>
                    <div class="form-content">
                        <div class="form" style="width: 300px; display: inline-block;float: left">
                            <form id="form-proposal" class="create" action="./create/proposal" enctype="multipart/form-data"
                                  method="post">
                                <select form="form-proposal" name="procedure_id" >
                                    <?php
                                    while ($row = get('Database')->row()) {
                                        echo '<option value="' . $row[Procedure::$field_id] . '">' . $row[Procedure::$field_title] . '</option>';
                                    }
                                    ?>
                                </select>
                                <input form="form-proposal" type="text" name="subject" placeholder="Subject">
                                <input form="form-proposal" type="text" name="requirements" placeholder="Venue">
                                <input form="form-proposal" type="text" name="budget" placeholder="Budget">
                                <textarea form="form-proposal" type="text" name="details" placeholder="Details about this file"></textarea>
                                <!--            <label class="checkbox"><input form="file" type="checkbox" name="visibility"> Draft</label>-->
                                <input form="form-proposal" type="file" name="attach" id="attach" accept="application/pdf"
                                       placeholder="Attach file">
                                <input type="button" value="Preview" form="form-proposal" onclick="preview('form-proposal')">
                                <input form="form-proposal" type="submit" value="Submit">
                            </form>
                        </div>
                    </div>
                </div>
                <!-- 1.2 Form Advance -->
                <div class="form-container">
                    <div class="form-title">
                        Request Advance
                    </div>
                    <div class="form-content">
                        <div class="form">
                            <input id="subject" type="text" class="block" name="subject" form="proposals" placeholder="Subject" required>
                            <div id="error-subject" class="error"></div>
                            <textarea class="block" name="details" placeholder="Details of your request." form="proposals" required></textarea>
                            <div id="error-details" class="error"></div>
                            <div id="item-1" data-position="1" class="form_item">
                                <input id="description-1" name="item_description_1" type="text" class="block" placeholder="Description of items" required>
                                <input id="quantity-1" name="item_quantity_1" type="text" class="inline" placeholder="Quantity" style="width: 84px" required>
                                <input id="amount-1" name="item_amount_1" type="text" class="inline" placeholder="Amount per item" required>
                                <input id="total-amount-1" type="text" value="₹0" class="inline" style="text-align: right; width: 64px" readonly>
                                <input type="button" value="Delete" id="item-delete-1" onclick="item_delete()">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 1.3 Form Refund -->
                <div class="form-container">
                    <div class="form-title">
                        Request Refund
                    </div>
                    <div class="form-content">ok</div>
                </div>
            </div>
            <div id="preview" style="display: inline-block; max-width: 700px">
                <?php
                //form('gymkhana');
                ?>
            </div>
        </div>
        <script>
            $(document).ready(
                function () {
                    $('#main-menu').append('<span class="item" onclick="show(\'#file_form_container\')">Create new request</span>');
                    $('.form-title').on('click', function(){
                        $('.form-content').hide();
                        $(this).parent().find('.form-content').toggle();
                    });

                    $('form.create').on('submit', function(e) {
                        e.preventDefault();
                        var id = showAlert('Uploading', '<div style="text-align: center">Please Wait!!!</div>');
                        $(this).ajaxSubmit(
                            {
                                success: function(data) {
                                    $('#'+id).remove();
                                    if(data.length == 0) showAlert('', 'I dont know');
                                    else showAlert('Some error', data);
                                }
                            }
                        );
                        return false;
                    });
                }
            )
        </script>
    <?php
    }
    // 2. Delete
    // 2.1 Delete Procedure
    if ($user['del'] || (array_key_exists('home.delete.procedure', $user['perms']) && $user['perms']['home.delete.procedure'])) {
        ?>
        <div id="delete-procedure" class="element delete">
            <h1>Delete Procedure</h1>
            <?php
            get('Database')->select(
                array(Procedure::$name),
                array(
                    Procedure::$field_id,
                    Procedure::$field_title
                ),
                '',
                array()
            );
            while ($row = get('Database')->row()) {
                echo '<div onclick="delete_procedure(' . $row[Procedure::$field_id] . ')" id="person' . $row[Procedure::$field_id] . '" class="person"><div class="name">' . $row[Procedure::$field_title] . '</div></div>';
            }
            ?>
        </div>
        <script>
            $(document).ready(
                function () {
                    $('#main-menu').append('<span class="item" onclick="show(\'#delete-procedure\')">Delete procedure</span> ')
                }
            )
        </script>
    <?php
    }
    // 2.2 Delete User
    if ($user['del'] || (array_key_exists('home.delete.user', $user['perms']) && $user['perms']['home.delete.user'])) {
        ?>
        <div id="delete-user" class="element delete">
            <h1>Delete Users</h1>
            <?php
            get('Database')->select(
                Person::$name,
                array(
                    Person::$field_id,
                    Person::$field_name,
                    Person::$field_post,
                    Person::$field_group
                ),
                '',
                array()
            );

            while ($row = get('Database')->row()) {
                echo '<div onclick="delete_user(' . $row[Person::$field_id] . ')" id="person' . $row[Person::$field_id] . '" class="person"><div class="name">' . $row[Person::$field_name] . '</div><div class="post">' . $row[Person::$field_post] . ', ' . $row[Person::$field_group] . '</div></div>';
            }
            ?>
        </div>
        <script>
            $(document).ready(
                function () {
                    $('#main-menu').append('<span class="item" onclick="show(\'#delete-user\')">Delete user</span> ')
                }
            )
        </script>
    <?php
    }
}