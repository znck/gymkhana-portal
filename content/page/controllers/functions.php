<?php
/**
 * Developer: Rahul Kadyan
 * Date: 20/01/14
 * Time: 5:12 PM
 * Product: PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
if(!defined('xDEC')) exit;
function get_user_permissions(){
    if(get('Auth')->logged())
    if (isset($_SESSION['list_user_permissions'])) {
        return $_SESSION['list_user_permissions'];
    } else {
        get('Database')->select(
            Person::$name,
            array(Person::$field_power),
            'WHERE '.quot(Person::$field_id).'=?',
            array($_SESSION[USER_ID])
        );
        $user = get('Database')->row();
        $user['perms'] = (array)json_decode($user[Person::$field_power]);
        $perm = (array_key_exists('home.create.*', $user['perms']) && $user['perms']['home.create.*']) || (array_key_exists('home.*', $user['perms']) && $user['perms']['home.*']);
        $del = (array_key_exists('home.delete.*', $user['perms']) && $user['perms']['home.delete.*']) || (array_key_exists('home.*', $user['perms']) && $user['perms']['home.*']);
        $_SESSION['list_user_permissions'] = array(
            'perms' => $user['perms'],
            'all' => $perm,
            'del' => $del
        );
        return $_SESSION['list_user_permissions'];
    } else array(
        'perms' => false,
        'all' => false,
        'del' => false
    );
}