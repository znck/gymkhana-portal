<?php
/**
 * Developer: Rahul Kadyan
 * Date: 20/01/14
 * Time: 2:21 PM
 * Product: PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
if(!defined('xDEC')) exit;
if(get('Auth')->logged()) {
    get('Database')->select(
        Person::$name,
        '*',
        "WHERE ".quot(Person::$field_id)."=?",
        array(
            $_SESSION[USER_ID]
        )
    );
    $user = get('Database')->row();
    ?>
    <div class="line">
        <div class="user">
            <div id="main-menu" class="menu">
            </div>
            <span class="item" style="cursor: pointer; line-height: 28px; padding: 0 8px; border-right: 1px #000 solid;"><?php echo $user[Person::$field_name]; ?></span>
            <a href="./logout"> &nbsp; logout</a>
        </div>
    </div>
    <div id="response"></div> <?php
}