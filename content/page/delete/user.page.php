<?php
/**
 * Developer: Rahul Kadyan
 * Date: 20/01/14
 * Time: 8:27 PM
 * Product: PhpStorm
 * Copyright (C) 2013 Rahul Kadyan
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */
if(!defined('xDEC')) exit;
class delete_user extends Admin{

    function startOutput($var)
    {
        @require_once(CONTENT.'page/controllers/functions.php');
        $user = get_user_permissions();
        if ($user['del'] || (array_key_exists('home.delete.user', $user['perms']) && $user['perms']['home.delete.user'])) {
            if (isset($_GET['id']) && intval($_GET['id']) != 0) {
                get('Database')->delete(
                    quot(Person::$name),
                    "WHERE ".quot(Person::$field_id)."=?",
                    array(
                        intval($_GET['id'])
                    )
                );
                get('Database')->delete(
                    quot(Login::$name),
                    "WHERE ".quot(Login::$field_id)."=?",
                    array(
                        intval($_GET['id'])
                    )
                );
                echo 'user deleted :(';
            } else {
                echo 'Some error occurred :(';
            }
        } else echo 'You\'re not authorised for this action';
    }
}

set(PAGE_OBJECT, new delete_user());