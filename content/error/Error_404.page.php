<?php
if (!defined('xDEC')) exit;
class Error_404 extends Pages
{

    function startOutput($var)
    {
        header("HTTP/1.0 404 Not Found");
        get("Logger")->custom_log("404.log", "URL: " . get('REQUEST_URI') . PHP_EOL . "USER AGENT: " . (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : ''));
        parent::__head__($var);
        parent::__title__(' | Page not found');
        parent::__body__($var);
        ?>
        <div id="parent-container" class="center-text container text"><h1><span class="dark">404.</span>
                That's an error.</h1>
        </div>
        <div style="height: 128px"></div>
        <?php
        parent::end_body();
    }
}

set(PAGE_OBJECT, new Error_404());