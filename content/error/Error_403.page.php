<?php
if (!defined('xDEC')) exit;
class Error_403 extends Pages
{

    function startOutput($var)
    {
        header("HTTP/1.0 403 Forbidden");
        get("Logger")->custom_log("403.logs", "URL: " . get('REQUEST_URI') . PHP_EOL . "USER AGENT: " . (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '') . PHP_EOL . print_r($_SESSION, true));
        parent::__head__($var);
        parent::__title__(' | Forbidden');
        parent::__body__($var);
        ?>
        <div id="parent-container" class="center-text container text"><h1><span class="dark">403.</span> That's an
            error.</h1>

        <p style="text-align: left">You are not allowed to be here.</p>

        <p style="text-align: left">The requested URL <span
                style="color: #999; font-size: 0.8rem; word-break: break-all"><?php echo $_SERVER['REQUEST_URI']; ?></span>
            is forbidden<br><br>
        </p>

        <div style="height: 128px"></div>
        <?php
        parent::end_body();
    }
}

set(PAGE_OBJECT, new Error_403());